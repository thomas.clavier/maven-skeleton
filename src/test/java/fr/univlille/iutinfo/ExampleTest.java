package fr.univlille.iutinfo;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ExampleTest {
    @Test
    public void should_return_hello_world() {
        Assertions.assertEquals("Hello World!", new Example().hello());
    }
}