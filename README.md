# Comment utiliser ce squelette de projet maven ?

```bash
git clone https://gitlab.univ-lille.fr/thomas.clavier/maven-skeleton.git
git remote set-url origin https://gitlab.univ-lille.fr/prenom.nom.etu/mon-projet.git
```

Dans gitlab créer le nouveau projet `mon-projet` avant de faire le premier `git push`
